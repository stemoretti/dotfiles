package ste;

use strict;

require Vile::Exporter;

use vars qw(@ISA %REGISTRY);
@ISA      = 'Vile::Exporter';
%REGISTRY = (
    'mybackup' => [ \&my_backup, '' ],
    'flip-line-comment' => [ \&flip_line_comment, '' ]
);

#use File::Copy;
use POSIX qw(strftime);

sub my_backup {
    my $filename = $Vile::current_buffer->filename;

    return 0 unless (scalar $Vile::current_buffer->get("modified") eq "TRUE"
                     && $filename !~ '/^$/' && -f $filename && -r $filename);

    my $bkpfile = $filename;
    my $bkpdir = $ENV{HOME} . "/.vile/bkp";

    unless (-e $bkpdir) {
        print "[Error: " . $bkpdir . " does not exists.]";
        return undef;
    }

    unless (-d $bkpdir) {
        print "[Error: " . $bkpdir . " is not a directory.]";
        return undef;
    }

    unless (-w $bkpdir) {
        print "[Error: Directory " . $bkpdir . " is not writable.]";
        return undef;
    }

    $bkpfile =~ s!^/!!;
    $bkpfile =~ s!/!%!g;
    my $mtime = (stat($filename))[9];

    my $fdate = strftime ".%Y.%m.%d-%H.%M", localtime($mtime);

    if (system('cp', '-a', $filename, $bkpdir . "/" . $bkpfile . $fdate) == -1) {
        print "[Error: Backup failed: $!]";
        return undef;
    }
}

Vile::register_motion 'first-lcase-word-back' => sub {
    my $cb = $Vile::current_buffer;
    my $bword = $cb->set_region('b')->fetch;

    $bword =~ s/\s+$//;
    if ($bword =~ m/[0-9a-zA-Z]*[a-z]+[_0-9A-Z]*$/g) {
        my $chnum = length($bword) - length($&);

        $cb->motion('b');
        if ($chnum > 0) {
            $cb->motion($chnum . 'l');
        }
        $cb->dot($cb->dot);
    }
};

sub flip_line_comment {
    my $cb = $Vile::current_buffer;

    my ($row, $col) = $cb->dot();
    my $cline = $cb->set_region($row, $row)->fetch;
    $cb->dot($row, 0);
    if ($cline !~ m!^\s*$!) {
        if ($cline =~ m!(^\s*)//!) {
            my $len = length $1;
            if ($len) {
                $cb->motion("${len}l");
            }
            $cb->set_region("2l")->delete;
        } else {
            print $cb "//";
        }
    }
    $cb->motion("j");
    $cb->motion("^");
    $cb->dot($cb->dot);
};

1;