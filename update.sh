#!/bin/bash

DOTFILES_PATH="$HOME/file_ste/_bitbucket_stemoretti/dotfiles"

_clean_pinerc() {
    oldifs=$IFS
    IFS=" "

    while read -r line; do
        if [[ "$line" =~ ^"$1".* ]]; then
            echo "$1"
            echo
            while read -r line; do
                if [[ "$line" =~ ^#.* ]]; then
                    echo "$line"
                    break
                fi
            done
        else
            echo "$line"
        fi
    done > "${DOTFILES_PATH}/.pinerc.tmp" < "${DOTFILES_PATH}/.pinerc"
    mv "${DOTFILES_PATH}/.pinerc.tmp" "${DOTFILES_PATH}/.pinerc"

    IFS=$oldifs
}

_copy_file() {
    for f in "${@:2}"; do
        cp -a "$HOME/$1/$f" "$DOTFILES_PATH/$1/${f%/*}"
    done
}

_copy_dir() {
    cp -a $HOME/$1/* "$DOTFILES_PATH/$1"
}

_copy_file "." \
    .bash_logout \
    .bashrc \
    .gitconfig \
    .inputrc \
    .manpath \
    .pinerc \
    .profile \
    .qtcreatorrc \
    .qtdevenv.sh \
    .screenrc \
    .tmux.conf \
    .vile.keywords.x11 \
    .vilerc \
    .virtual.sh \
    .xkb.sh \
    .Xresources \
    .xsessionrc \
    .vile/perl/ste.pm \
    .vile/vile.keywords

# remove personal information from pinerc
cp -a "$DOTFILES_PATH/.pinerc" "$DOTFILES_PATH/.pinerc.bkp"
_clean_pinerc 'folder-collections=.mail/[]'
_clean_pinerc 'patterns-roles='
_clean_pinerc 'xoauth2-info='

_copy_dir .config/nvim
_copy_dir .config/procps
_copy_dir .xkb

# fix vim local plugins
sed -i 's|~/file_ste/_github_||' $DOTFILES_PATH/.config/nvim/init.vim

_copy_file "local-bin" \
    alpine \
    chromium \
    clangd \
    efm-langserver \
    ffmpeg \
    fontforge \
    go \
    gofmt \
    history_merge.bash \
    inkscape \
    links \
    linstall \
    mupdf \
    mupdf-gl \
    nvim \
    qpdfview \
    shellcheck \
    trans \
    valgrind \
    vile \
    vilex \
    vim \
    xterm

_copy_dir local-install/.install-scripts
