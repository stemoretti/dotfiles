" Vim color file
" Based on https://www.vim.org/scripts/script.php?script_id=573

set background=dark
hi clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name="ste"

hi Normal	cterm=none		gui=none		guibg=#000000	guifg=#bbbbbb
hi Cursor	cterm=none					guibg=#ffffff	guifg=#000000
hi link CursorIM Cursor
hi Directory	cterm=bold		ctermfg=blue		gui=bold	guifg=royalblue1
hi DiffAdd	ctermbg=blue		ctermfg=yellow		guibg=#080888	guifg=#ffff00
hi DiffDelete	ctermbg=black		ctermfg=darkgray	guibg=#080808	guifg=#444444
hi DiffChange	ctermbg=black					guibg=#080808	guifg=#ffffff
hi DiffText	ctermbg=black		ctermfg=darkred		guibg=#080808	guifg=#bb0000
hi ErrorMsg	ctermbg=darkred		ctermfg=white		guibg=#bb0000	guifg=#ffffff
hi Folded	ctermbg=black		ctermfg=darkblue	guibg=#000000	guifg=#0000bb
hi link FoldColumn Folded
hi IncSearch	ctermbg=black		ctermfg=gray		guibg=#000000	guifg=#bbcccc
hi LineNr	ctermbg=232		ctermfg=darkgray	guibg=#080808	guifg=#444444
hi ModeMsg				ctermfg=white				guifg=#ffffff
hi MoreMsg				ctermfg=green				guifg=#44ff44
hi NonText				ctermfg=blue				guifg=#4444ff
hi Question				ctermfg=yellow				guifg=#ffff00
hi Search	ctermbg=NONE		ctermfg=green		guibg=NONE	guifg=#00ff00
hi SpecialKey				ctermfg=blue				guifg=#4444ff
hi StatusLine	cterm=none	ctermbg=darkcyan	ctermfg=black	gui=none	guibg=#00aaaa	guifg=#000000
hi StatusLineNC	cterm=none	ctermbg=gray		ctermfg=black	gui=none	guibg=#bbbbbb	guifg=#000000
hi Title	cterm=bold			ctermfg=white	gui=bold			guifg=#ffffff
hi VertSplit	cterm=none	ctermbg=none	ctermfg=gray	gui=none	guibg=NONE	guifg=#bbcccc
hi Visual	cterm=nocombine	ctermbg=gray	ctermfg=black	gui=nocombine	guibg=#bbbbbb	guifg=#000000
hi link VisualNOS Visual
hi WarningMsg				ctermfg=yellow				guibg=#000000	guifg=#ffff00
hi link WildMenu StatusLine
"hi Menu
"hi Scrollbar
"hi Tooltip

" syntax highlighting groups
hi Comment	ctermfg=blue	cterm=bold	guifg=royalblue1	gui=bold
hi Constant	ctermfg=darkcyan		guifg=darkcyan
hi Identifier 	ctermfg=white	cterm=bold	guifg=#ffffff		gui=bold
hi Statement 	ctermfg=cyan	cterm=bold	guifg=cyan		gui=bold
hi PreProc	ctermfg=darkcyan		guifg=darkcyan
hi Type		ctermfg=white	cterm=bold	guifg=#ffffff		gui=bold
hi Special	ctermfg=blue	cterm=bold	guifg=royalblue1	gui=bold
hi Underlined	ctermfg=blue			guifg=royalblue1
hi Ignore	ctermfg=darkgray		guifg=#444444
hi Error	ctermbg=black			ctermfg=darkred		guibg=#000000	guifg=#ff0000
hi Todo		ctermbg=darkred			ctermfg=yellow		guibg=#aa0006	guifg=#fff300

hi SignColumn	ctermbg=232	guibg=#080808

hi CursorLine	cterm=none	ctermbg=232	gui=none	guibg=#080808
hi CursorLineNr	cterm=bold	ctermfg=gray	ctermbg=232	gui=bold	guifg=#bbcccc	guibg=#080808

hi Pmenu	ctermbg=233	ctermfg=gray	guibg=#0e0e0e	guifg=#ffffff
hi PmenuSel	ctermbg=white	ctermfg=black	guibg=#ffffff	guifg=#000000
hi PmenuSbar	ctermbg=238			guibg=#666666
hi PmenuThumb	ctermbg=white			guibg=#ffffff

hi NormalFloat	ctermbg=233	ctermfg=gray	guibg=#0e0e0e	guifg=#ffffff

hi SpellBad	ctermbg=none	ctermfg=red	gui=undercurl	guisp=#ff6666
hi SpellCap	ctermbg=none	ctermfg=blue	gui=undercurl	guisp=#6666ff
hi SpellRare	ctermbg=none	ctermfg=magenta	gui=undercurl	guisp=#ff66ff

hi TabLine	cterm=none	ctermbg=233	ctermfg=darkcyan	gui=none	guibg=#0e0e0e	guifg=darkcyan
hi TabLineSel	cterm=none	ctermbg=darkcyan	ctermfg=black	gui=none	guibg=#00aaaa	guifg=#000000
hi TabLineFill 	cterm=none	ctermbg=233	ctermfg=none	gui=none	guibg=#0e0e0e	guifg=#000000

hi MatchParen	cterm=bold	ctermbg=235	ctermfg=green	gui=bold	guibg=#444444	guifg=#44ff44

" Statusline
hi WarningStatusline ctermfg=black ctermbg=darkyellow guifg=#000000 guibg=#fff300
hi ErrorStatusline ctermfg=white ctermbg=darkred guifg=#ffffff guibg=#bb0000

" Plugins
hi LspHintText ctermfg=green ctermbg=232 guifg=#00bb00 guibg=#080808
hi LspHintHighlight cterm=underline ctermfg=green gui=undercurl guisp=#00ff00
hi LspWarningText ctermfg=yellow ctermbg=232 guifg=#fff300 guibg=#080808
hi LspWarningHighlight cterm=underline ctermfg=yellow gui=undercurl guisp=#ffff00
hi LspErrorText ctermfg=red ctermbg=232 guifg=#bb0000 guibg=#080808
hi LspErrorHighlight cterm=underline ctermfg=red gui=undercurl guisp=#ff0000
hi LspSemanticClass cterm=none ctermfg=white
hi LspSemanticEnum cterm=none ctermfg=white
hi LspSemanticEnumMember ctermfg=green
hi LspSemanticFunction cterm=none
hi LspSemanticMethod cterm=none
hi LspSemanticNamespace ctermfg=white
hi LspSemanticOperator cterm=none
hi LspSemanticParameter cterm=none
hi LspSemanticProperty cterm=none
hi LspSemanticType cterm=none ctermfg=white
hi LspSemanticVariable cterm=none
hi lspReference ctermbg=236 guibg=#333333
" hi PopupWindow ctermbg=232 guibg=#080808

hi BuflistPopupNormal cterm=none ctermfg=white ctermbg=232 guifg=#ffffff guibg=#080808
hi BuflistPopupBorder cterm=none ctermfg=235 ctermbg=232 guifg=#262626 guibg=#080808
hi BuflistPopupSel cterm=none ctermfg=white ctermbg=236 guifg=#ffffff guibg=#484848
