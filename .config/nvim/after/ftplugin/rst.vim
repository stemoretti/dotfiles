nmap <buffer> <silent> <F2> :call <SID>RstUnderlineHeading()<CR>

function! s:RstUnderlineHeading()
    let chars = ['#', '*', '=', '-', '^', '"']
    let l:line = line('.')
    let l:llength = strlen(getline(l:line))
    if l:llength == 0
        return
    endif
    echo 'Enter heading level character (' .. join(chars, ', ') .. ')'
    let l:ch = getchar()
    if type(l:ch) != v:t_number || l:ch == 27
        return
    endif
    let l:char = nr2char(l:ch)
    if match(join(chars), l:char) < 0
        echohl ErrorMsg
        echo '"' . l:char . '" is not a valid heading level character'
        echohl None
        return
    endif
    call append(l:line, repeat(l:char, l:llength))
endfunction
