setlocal completeopt=menu,menuone,noinsert
setlocal textwidth=88

let g:pyindent_open_paren = 4

" let b:commentary_startofline = 1

let NERDTreeIgnore = [ '.pyc$', '__$', '^venv$' ]
