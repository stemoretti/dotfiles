setlocal cinoptions=j1,(0,ws,Ws,m1,i0,g0,l1,:0,N-s,E-s
setlocal commentstring=//\ %s
setlocal completeopt=menu,menuone,noinsert
setlocal expandtab
setlocal shiftwidth=4

imap <buffer> <C-s><C-p> {<NL>}<C-\><C-o>O

" nnoremap <buffer> <silent> <F4> :LspDocumentSwitchSourceHeader<CR>
