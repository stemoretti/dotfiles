"{{{1 === settings ===

if has('nvim')
    if has('win32')
        set guifont=DejaVu\ Sans\ Mono:h14
    endif
    augroup Startup
        autocmd!
        " When editing a file, always jump to the last known cursor position.
        autocmd BufReadPost *
        \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ |   exe "normal! g`\""
        \ | endif
    augroup END
else
    unlet! skip_defaults_vim
    source $VIMRUNTIME/defaults.vim

    if &term =~ "screen."
        let &t_SI = "\<Esc>P\<Esc>[6 q\<Esc>\\"
        let &t_SR = "\<Esc>P\<Esc>[4 q\<Esc>\\"
        let &t_EI = "\<Esc>P\<Esc>[2 q\<Esc>\\"
        set mouse-=a
    else
        let &t_SI = "\<Esc>[6 q"
        let &t_SR = "\<Esc>[4 q"
        let &t_EI = "\<Esc>[2 q"
    endif

    set autoread
    set belloff=all
    set complete-=i
    set dir=~/.vim/swap
    set formatoptions+=j
    set hlsearch
    set scrolloff=0
    set shortmess+=F
    set shortmess-=S
    set viminfofile=~/.vim/cache/viminfo

    if has('gui_running')
        set guifont=DejaVu\ Sans\ Mono\ 14
        set guicursor+=a:blinkon0
        set guioptions=airL
    endif
endif

set autoindent
set completeopt=menu,longest
set expandtab
set foldmethod=marker
set hidden
set number
set noruler
set shiftwidth=4
set nosmarttab
set nostartofline
set notimeout
set wildmode=longest,list

augroup FileTypeCustomizations
    autocmd!
    autocmd FileType vim let g:vim_indent_cont = &sw
    autocmd FileType html exec 'hi link javaScript Normal'
augroup END

augroup PreserveWinView
    autocmd!
    " When switching buffers, preserve window view.
    autocmd BufLeave * call s:AutoSaveWinView()
    autocmd BufEnter * call s:AutoRestoreWinView()
augroup END

augroup CursorLine
    autocmd!
    autocmd VimEnter,WinEnter,BufWinEnter * setlocal cursorline
    autocmd WinLeave * setlocal nocursorline
augroup END

runtime statusline.vim

colorscheme ste

"{{{1 === mappings ===

nmap <C-k> k

nmap Q :confirm qall<CR>
nmap <silent> <C-w>c :call <SID>SafeCloseWindow()<CR>

" buffer management
nmap <C-s><C-j> :ls<CR>:b<Space>
nmap <silent> <C-s>k :call <SID>KillBuffer()<CR>

imap <C-f> <Right>
imap <C-b> <Left>

imap <C-u> <C-\><C-o>:call <SID>UpcaseToStartOfWord()<CR>

"{{{1 === functions ===

function! s:nerdtree_isopen()
    return exists("g:NERDTree") && g:NERDTree.IsOpen()
endfunction

function! s:KillBuffer(...) abort
    let curbuf = a:0 ? a:1 : bufnr()
    let bufinfo = getbufinfo(curbuf)[0]

    if !bufinfo.listed
        let open_windows = len(getwininfo())
        if open_windows > 2 || (open_windows == 2 && !s:nerdtree_isopen())
            close
        else
            bprevious
        endif
        return
    endif

    if len(getbufinfo({ 'buflisted': 1 })) == 1
        echohl ErrorMsg | echo "Cannot kill the only buffer!" | echohl None
        return
    endif

    if bufinfo.changed
        let ans = confirm("Save changes?", "&Yes\n&No\n&Cancel", 3, "Q")
        let buffer_name = bufname(curbuf)
        if ans == 1
            if empty(buffer_name)
                let filename = input("Enter filename: ")
                if empty(filename)
                    return
                else
                    exec "write" filename
                endif
            else
                write
            endif
            mode
            echo "Buffer '" . buffer_name . "' killed. Changes saved"
        elseif ans == 2
            mode
            echo "Buffer '" . buffer_name . "' killed. Changes discarded"
        else
            mode
            echo "Killing of buffer '" . buffer_name . "' canceled"
            return
        endif
    endif

    while len(win_findbuf(curbuf)) > 1
        close
        call win_gotoid(win_findbuf(curbuf)[0])
    endwhile

    if bufnr() == curbuf && len(getwininfo()) == 2 && s:nerdtree_isopen()
        bprevious
    endif

    exec "bdelete!" curbuf
endfunction

function! s:UpcaseToStartOfWord()
    let line = getline('.')
    let col = col('.')
    if col != 1 && match(line[col - 2], '\W')
        if col == 2 && len(line) == 1
            normal! gU$$
        elseif col > len(line)
            if match(line[col - 3], '\W')
                normal! bgU$$
            else
                normal! hgU$$
            endif
        else
            normal! gUb`^
        endif
    endif
endfunction

function! s:SafeCloseWindow()
    let open_windows = len(getwininfo())
    if open_windows == 1 || (open_windows == 2 && s:nerdtree_isopen())
        echohl ErrorMsg | echo "Cannot close the last window!" | echohl None
        return
    endif
    close
endfunction

function! s:DownloadFile(from, to)
    if empty(glob(a:to))
        exec '!curl' '-sSfLo' a:to '--create-dirs' a:from
        if !v:shell_error
            echomsg 'Downloaded file' a:to
            return 1
        endif
        echomsg 'Failed to download file' a:from
    endif
    return 0
endfunction

" https://vim.fandom.com/wiki/Avoid_scrolling_when_switch_buffers
" Save current view settings on a per-window, per-buffer basis.
function! s:AutoSaveWinView()
    if !exists("w:SavedBufView")
        let w:SavedBufView = {}
    endif
    let w:SavedBufView[bufnr("%")] = winsaveview()
endfunction

" Restore current view settings.
function! s:AutoRestoreWinView()
    let buf = bufnr("%")
    if exists("w:SavedBufView") && has_key(w:SavedBufView, buf)
        let v = winsaveview()
        let atStartOfFile = v.lnum == 1 && v.col == 0
        if atStartOfFile && !&diff
            call winrestview(w:SavedBufView[buf])
        endif
        unlet w:SavedBufView[buf]
    endif
endfunction

"{{{1 === plugins ===

"{{{2 settings

let g:startify_session_sort = 1
let g:startify_enable_special = 0
let g:startify_custom_header = []
let g:startify_bookmarks = [
    \ { 'c': '~/.config/nvim/init.vim' },
    \ { 'w': '~/file_ste/words.txt' },
    \ ]
let g:startify_lists = [
    \ { 'type': 'sessions',  'header': ['   Sessions']       },
    \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
    \ { 'type': 'files',     'header': ['   MRU']            },
    \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
    \ { 'type': 'commands',  'header': ['   Commands']       },
    \ ]

let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_diagnostics_signs_error = { 'text': '◯' }
let g:lsp_diagnostics_signs_warning = { 'text': '△' }
let g:lsp_diagnostics_signs_hint = { 'text': '□' }
let g:lsp_diagnostics_signs_information = { 'text': 'ℹ' }
let g:lsp_document_code_action_signs_enabled = 0
if has('nvim')
    let g:lsp_signature_help_enabled = 0
else
    let g:lsp_use_native_client = 1
    " let g:lsp_inlay_hints_enabled = 1
    let g:lsp_inlay_hints_mode = {
        \ 'normal': ['always', '!curline'],
        \ 'insert': ['always', '!curline'],
        \ }
    let g:lsp_diagnostics_virtual_text_align = 'after'
    let g:lsp_diagnostics_virtual_text_wrap = 'truncate'
endif
" let g:lsp_semantic_enabled = 1
" let g:lsp_semantic_delay = 1000
" let g:lsp_use_native_client = 1

let g:lsp_settings = {
    \ 'clangd': { 'args': ['--header-insertion=never', '-j=2', '--malloc-trim'] },
    \ 'efm-langserver': { 'disabled': 0, 'allowlist': ['sh', 'bash', 'rst', 'qml'] },
    \ }

" Don't load netrw
let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1

let g:NERDTreeMinimalUI = 1
let g:NERDTreeQuitOnOpen = 1
let g:NERDTreeCustomOpenArgs = { 'file': { 'where': 'p', 'keepopen': 1 } }

let g:buflist_popup_split_path = 1
augroup BuflistPopupColors
    autocmd!
    if has('nvim')
        autocmd User BuflistPopup
            \ call nvim_win_set_option(buflist_popup#nvim#winid(),
            \ 'winhighlight',
            \ 'Normal:BuflistPopupNormal'
            \ .. ',CursorLine:BuflistPopupSel'
            \ .. ',FloatBorder:BuflistPopupBorder')
    else
        autocmd User BuflistPopup
            \ call popup_setoptions(buflist_popup#vim#winid(), {
                \ 'borderhighlight': ['BuflistPopupBorder'],
                \ 'highlight': 'BuflistPopupNormal',
                \ 'scrollbarhighlight': 'BuflistPopupNormal',
                \ 'thumbhighlight': 'BuflistPopupSel',
                \ })
    endif
augroup END

"{{{2 loading

let s:data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
let s:ghuc = 'https://raw.githubusercontent.com'

if s:DownloadFile(s:ghuc . '/junegunn/vim-plug/master/plug.vim',
    \ s:data_dir . '/autoload/plug.vim')
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call s:DownloadFile(s:ghuc . '/Vimjas/vim-python-pep8-indent/master/indent/python.vim',
    \ s:data_dir . '/indent/python.vim')

call plug#begin((has('nvim') ? stdpath('data') : '~/.vim') . '/plugged')

Plug 'stemoretti/vim-buflist-popup'

Plug 'preservim/nerdtree'
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-commentary'
Plug 'habamax/vim-rst'
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'

call plug#end()

"{{{2 mappings

nmap <C-_> <Plug>CommentaryLine<CR>

function! SaveBuffer() abort
    let buf_index = buflist_popup#get_current_index()
    let buf_bufnr = buflist_popup#at(buf_index).bufnr
    BuflistPopupClose
    let curbuf = bufnr('%')
    exec buf_bufnr .. ',' .. buf_bufnr .. 'bufdo :w'
    exec curbuf .. 'b'
    BuflistPopupShow
    BuflistPopupSelect buf_index
endfunction

function! SaveAllModifiedBuffers() abort
    let buf_index = buflist_popup#get_current_index()
    BuflistPopupClose
    let curbuf = bufnr('%')
    for bufinfo in buflist_popup#list()
        if bufinfo.modified
            exec bufinfo.bufnr .. ',' .. bufinfo.bufnr .. 'bufdo :w'
        endif
    endfor
    exec curbuf .. 'b'
    BuflistPopupShow
    BuflistPopupSelect buf_index
endfunction

function! DeleteBuffer() abort
    if buflist_popup#size() < 2 | return | endif
    let buf_index = buflist_popup#get_current_index()
    BuflistPopupClose
    call <SID>KillBuffer(buflist_popup#at(buf_index).bufnr)
    BuflistPopupShow
    BuflistPopupSelect buf_index
endfunction

function! OpenBuffer(command) abort
    BuflistPopupClose
    exec a:command
    exec "b" buflist_popup#at(buflist_popup#get_current_index()).bufnr
endfunction

let g:buflist_popup_mappings = [
    \ {'key': 's', 'exec': ':call SaveBuffer()'},
    \ {'key': 'S', 'exec': ':call SaveAllModifiedBuffers()'},
    \ {'key': 'd', 'exec': ':call DeleteBuffer()'},
    \ {'key': 'h', 'exec': ':call OpenBuffer(":split")'},
    \ {'key': 'v', 'exec': ':call OpenBuffer(":vsplit")'},
    \ ]
if has('nvim')
    let g:buflist_popup_mappings = g:buflist_popup_mappings + [
        \ {'key': '<M-0>', 'exec': ''},
        \ {'key': '<C-^>', 'exec': ''},
        \ ]
endif

nmap <silent> <M-0> :NERDTreeToggle<CR>
if !has('nvim')
    nmap <silent> <Esc>0 :NERDTreeToggle<CR>
endif
augroup NERDTree
    autocmd!
    autocmd FileType nerdtree
        \ exec 'nnoremap <buffer> _ <Nop>' |
        \ exec 'nnoremap <buffer> <C-w>c <Nop>' |
        \ exec 'nnoremap <buffer> <C-s><C-j> <Nop>' |
        \ exec 'nnoremap <buffer> <C-s>k <Nop>'
augroup END

function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    nmap <buffer><silent> gl :call <SID>toggle_lsp_diagnostics()<CR>
    nnoremap <buffer> <expr><c-s><c-n> lsp#scroll(+4)
    nnoremap <buffer> <expr><c-s><c-p> lsp#scroll(-4)

    imap <buffer> <C-Space> <C-x><C-o>
    if !has('nvim')
        imap <buffer> <C-@> <C-x><C-o>
    endif

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')

    call lsp#disable_diagnostics_for_buffer()

    let b:lsp_lint_enabled = 0

    function! s:toggle_lsp_diagnostics() abort
        if b:lsp_lint_enabled
            call lsp#disable_diagnostics_for_buffer()
            let b:lsp_lint_enabled = 0
            echo "lsp diagnostics disabled"
        else
            call lsp#enable_diagnostics_for_buffer()
            let b:lsp_lint_enabled = 1
            echo "lsp diagnostics enabled"
        endif
    endfunction

    if has('nvim')
        inoremap <buffer> <expr> <cr> pumvisible()
            \ ? "\<c-y>" : <SID>floating_windows()
            \ ? "\<c-\><c-o>:call lsp#ui#vim#output#closepreview()<cr><cr>" : "\<cr>"
    else
        inoremap <buffer> <expr> <cr> pumvisible() ? "\<c-y>" : "\<cr>"
    endif

    function! s:floating_windows() abort
        " https://github.com/neovim/neovim/issues/12389
        let open_windows = len(nvim_list_wins())
        let open_no_float_windows = len(filter(nvim_list_wins(), {k,v->nvim_win_get_config(v).relative==""}))
        return open_windows != open_no_float_windows
    endfunction

    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END
