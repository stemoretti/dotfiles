#!/usr/bin/env bash

DEVQT_QT_DIR=$HOME/local-install/qt
DEVQT_JAVA_PATH=$HOME/local-install/java/current
DEVQT_ANDROID_SDK_PATH=$HOME/local-install/android-sdk
DEVQT_ANDROID_NDK_PATH=$DEVQT_ANDROID_SDK_PATH/ndk/25.1.8937393

OLDPATH=$PATH

_pathmunge() {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ]; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
    esac
}

_setqt() {
    local cur prev words cword
    _init_completion -n = || return

    if [ "${#words[@]}" == "2" ]; then
        local names=$(basename -a "${DEVQT_QT_DIR}"/[56]*)
    elif [ "${#words[@]}" == "3" ]; then
        local names=$(basename -a $(ls -d "${DEVQT_QT_DIR}"/"$prev"/*/ | grep -v Src))
    else
        local names=
    fi

    COMPREPLY=($(compgen -W "$names" -X "$cur" -- "$cur"))
} && complete -F _setqt setqt

_devqt_help() {
    echo "Usage: setqt [OPTIONS] [VERSION] [ARCH]"
    echo "Positional argument:"
    echo "VERSION                    Qt version."
    echo "ARCH                       Qt architecture."
    echo "Optional arguments:"
    echo "-l, --list                 List all available versions."
    echo "-c, --clear                Clear variables."
}

_devqt_clear() {
    export PATH=$OLDPATH
    unset ANDROID_NDK_ROOT
    unset ANDROID_SDK_ROOT
    unset ANDROID_NDK
    unset ANDROID_SDK
    unset JAVA_HOME
    unset Qt_DIR
    unset Qt_HOST_DIR
    unset Qt_armeabi_v7a_DIR
    unset Qt_arm64_v8a_DIR
    unset Qt_x86_DIR
    unset Qt_x86_64_DIR
}

_devqt_list() {
    echo -e "List of Qt installations under ${DEVQT_QT_DIR}:\n"
    for qt in "${DEVQT_QT_DIR}"/[56]*; do
        basename "${qt}"
        for arch in "${qt}"/*; do
            if [ -d "$arch" ]; then
                echo -n "    "
                basename "${arch}"
            fi
        done
    done
}

setqt() {
    if [ $# -eq 0 ]; then
        _devqt_help
    elif [ $# -le 3 ]; then
        case "${1}" in
            -c|--clear) echo "Reset path"; _devqt_clear;;
            -l|--list) _devqt_list;;
            -*)
                echo "Unrecognized option: ${1}"
                _devqt_help
                ;;
            *)
                _devqt_clear
                qt_dir=$DEVQT_QT_DIR/$1
                arch="gcc_64"
                if [ $# -eq 2 ]; then
                    arch=$2
                fi
                arch_dir=$qt_dir/$arch
                export Qt_DIR=$arch_dir
                if [ -d "$arch_dir" ]; then
                    _pathmunge "$arch_dir/bin"
                    export PKG_CONFIG_PATH=$arch_dir/lib/pkgconfig
                    echo "Qt version $1/$arch set in path"
                else
                    echo "Qt version $1/$arch not found"
                    return 1
                fi
                if [[ $arch =~ ^android.* ]]; then
                    export ANDROID_NDK_ROOT=$DEVQT_ANDROID_NDK_PATH
                    export ANDROID_SDK_ROOT=$DEVQT_ANDROID_SDK_PATH
                    export ANDROID_NDK=$ANDROID_NDK_ROOT
                    export ANDROID_SDK=$ANDROID_SDK_ROOT
                    export JAVA_HOME=$DEVQT_JAVA_PATH
                    export Qt_HOST_DIR=$qt_dir/gcc_64
                    export Qt_armeabi_v7a_DIR=$qt_dir/android_armv7
                    export Qt_arm64_v8a_DIR=$qt_dir/android_arm64_v8a
                    export Qt_x86_DIR=$qt_dir/android_x86
                    export Qt_x86_64_DIR=$qt_dir/android_x86_64
                    _pathmunge "$qt_dir/gcc_64/bin"
                    _pathmunge "$ANDROID_SDK_ROOT/cmdline-tools/latest/bin"
                fi
                ;;
        esac
    fi
}
