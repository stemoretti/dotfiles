===================
Configuration files
===================

.. contents:: Table of Contents
    :depth: 2

.. sectnum::

System configuration
====================

Linux Mint configuration.

plocate
-------

Uninstall plocate:

.. code:: console

    $ sudo apt-get purge plocate

sudo
----

Configure sudo so that it doesn't ask the current user for the password
by running the following command:

.. code:: console

    echo "$USER ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/$USER

Mouse pointer
-------------

Install the classical X11 mouse cursor theme:

.. code:: console

    $ sudo apt-get install xcursor-themes
    $ sudo update-alternatives --config x-cursor-theme

DNS servers
-----------

Use OpenDNS servers.

#. Install dnsmasq:

    .. code:: console

        $ sudo apt-get install dnsmasq

#. Write the following lines into the file ``/etc/dnsmasq.conf``:

    .. code:: cfg

        port=53
        domain-needed
        bogus-priv
        no-resolv
        no-poll
        server=208.67.222.222#443
        listen-address=127.0.0.1
        bind-interfaces
        no-hosts
        cache-size=150

#. Configure the system to use the local DNS server by following these steps:

    * Right click the connection systray icon and select **Edit connections...**
    * Select the current network and click on the settings icon
    * Select the **IPv4 Settings** tab
    * Set **Method** to **DHCP addresses only**
    * Set the DNS server to 127.0.0.1

#. To verify that everything works go to `InternetBadGuys.com`_.

#. To temporarily use the DNS server of the ISP:

    * In the IPv4 Settings tab replace the local DNS address with 192.168.1.1
    * Restart the networking

.. _`InternetBadGuys.com`: http://www.internetbadguys.com

Video card driver
-----------------

``/etc/X11/xorg.conf``:

.. code:: cfg

    Section "Device"
          Identifier  "Intel Graphics"
          Driver      "intel"
          Option      "TearFree"    "true"
    EndSection

XDG folders
-----------

Remove XDG folders in ``$HOME`` and run this command:

.. code:: console

    $ xdg-user-dirs-update

Create the folder ``$HOME/.config/desktop``, open the file
``$HOME/.config/user-dirs.dirs`` and change the Desktop entry to point to it.
Log out and back in to apply the modifications.

Applications
============

Ungoogled chromium
------------------

Download the latest AppImage from the `Ungoogled Chromium Binaries`_ page.

.. _`Ungoogled Chromium Binaries`: https://github.com/ungoogled-software/ungoogled-chromium-portablelinux/releases

Extensions
^^^^^^^^^^

    - `Chromium Web Store`_
    - `Improve YouTube!`_
    - `Browserpass`_ - `browserpass-native`_
    - `Calm Twitter`_
    - `Google Translate`_

.. _`Chromium Web Store`: https://github.com/NeverDecaf/chromium-web-store
.. _`Improve YouTube!`: https://chrome.google.com/webstore/detail/improve-youtube-video-you/bnomihfieiccainjcjblhegjgglakjdd?hl=en
.. _`Browserpass`: https://chrome.google.com/webstore/detail/browserpass/naepdomgkenhinolocfifgehidddafch?hl=en
.. _`browserpass-native`: https://github.com/browserpass/browserpass-native
.. _`Calm Twitter`: https://chrome.google.com/webstore/detail/calm-twitter/cknklikacoaeledfaldmhabmldkldocj
.. _`Google Translate`: https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb

Dark Mode
^^^^^^^^^

.. code:: console

    $ dconf write /org/gnome/desktop/interface/color-scheme \'prefer-dark\'

Qt Creator
----------

Enable android compilation by adding, in the Devices tab, the Java JDK location
and the android SDK/NDK.

Copy custom syntax highlighting and code styles.

#. Environment:

   - System:

     - theme flat dark

   - Keyboard:

     - unbind ctrl+k

#. Text Editor:

   - Fonts and Colors:

     - font dejavu sans mono size 14

#. Behavior:

   - Display:

     - disable animate matching parentheses
     - display right margin at 80

#. Completion:

   - activate completion manually
   - disable animate automatically inserted text

#. FakeVim:

   - General:

     - use fakevim
     - expand tabulators
     - shift width 4
     - read ``.vimrc`` from ``$HOME/.qtcreatorrc``

Others
------

.. csv-table::
    :header: "Name", "Description"
    :widths: 10,30

    mediainfo,command-line utility for reading information from audio/video files
