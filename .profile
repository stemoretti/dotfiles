#!/bin/bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTTIMEFORMAT="%F_%T  "
export HISTCONTROL="ignoreboth:erasedups"
export HISTFILESIZE=100

export EDITOR="vile"
export BROWSER="links"
export LESS="-i -R"

export QT_SELECT="default"
export QML_DISABLE_DISK_CACHE="true"

export GOPATH="$HOME/local-install/go/go_local"
export GOROOT="$HOME/local-install/go/current"

eval $(dircolors -b)
eval $(lesspipe)

[ -f /etc/profile.d/bash_completion.sh ] && source /etc/profile.d/bash_completion.sh

# Prompt
cyan="\[\033[0;36m\]"
bright_red="\[\033[1;31m\]"
bright_cyan="\[\033[1;36m\]"
white="\[\033[1;37m\]"
nocolor="\[\033[m\]"

prompt() {
    es=$?
    PS1="${cyan}[$nocolor\u$white@$nocolor\h: \W"
    if [ $es != 0 ]; then
        PS1+="$cyan|$bright_red$es"
    fi
    PS1+="$cyan] $bright_cyan\\$ $nocolor"
}

if [ $TERM != "dumb" ]; then
    PROMPT_COMMAND=prompt
    # case "$TERM" in
        # screen*) PROMPT_COMMAND+="; echo -ne '\ek\e\\'"
    # esac
else
    PS1="[\u@\h: \W] \$ "
fi

# Alias
alias rm='rm -vI'
alias mv='mv -vi'
alias cp='cp -vi'

alias grep='grep --color=auto'
alias free='free -h'

#ls family
alias ls='ls --color=auto -N'
alias l='ls -lh'
alias la='ls -A'
alias ll='l -A'
alias lld='ll --group-directories-first'
alias lt='l -tr'
alias lta='lt -A'

# ssh
alias keyon='ssh-add -t 10800'
alias keyoff='ssh-add -D'
alias keylist='ssh-add -l'

# End Alias

agentssh() {
    case $1 in
        'start')
            if [ -f ~/.ssh-agent ]; then
                echo "ssh agent is running (PID: $SSH_AGENT_PID)"
            else
                nohup ssh-agent -s | grep -v echo > ~/.ssh-agent
            fi
            source ~/.ssh-agent
        ;;
        'stop')
            if [ -f ~/.ssh-agent ]; then
                source ~/.ssh-agent
                kill -9 $SSH_AGENT_PID
                echo "ssh agent stopped"
                rm -rf ${SSH_AUTH_SOCK%/*}
                rm -f ~/.ssh-agent
            else
                echo "ssh agent is not running"
            fi
        ;;
        *) echo "usage $0 start|stop"
    esac
}

[ -f ~/.ssh-agent ] && source ~/.ssh-agent

pathmunge() {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ]; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
    esac
}

[ -f /etc/environment ] && source /etc/environment

pathmunge $HOME/local-bin
# pathmunge $HOME/local-install/texlive/2022/bin/x86_64-linux

[ -f ~/.qtdevenv.sh ] && source ~/.qtdevenv.sh

[ -f ~/.virtual.sh ] && source ~/.virtual.sh

hm() { source ~/local-bin/history_merge.bash; }
hd() { unset HISTFILE; }

use_clang() {
    local clang_dir="/home/ste/local-install/clang/current/bin"
    export CC=$clang_dir/clang
    export CXX=$clang_dir/clang++
    pathmunge $clang_dir
}

umask 0022

stty -ixon

# vile: sw=4
